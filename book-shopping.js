const bookFormEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');
const bookContainer = document.getElementById("books-container");
const listOfReading = document.getElementById("reading-list");


// const favBookFormEl = document
//book class - title suthor des
//book list - array of fav books funtion

class Book {
  constructor(title, author, isbn){
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

bookFormEl.addEventListener('submit', function(e) {
  e.preventDefault();
  const bookList = [];
  const readingList = [];
  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;
  const bookDate = `${year}-${month}-${date}`;
  const BASE_BOOK_URL =`https://api.nytimes.com/svc/books/v3/lists/${bookDate}/hardcover-fiction.json`;
  const bookURL = `${BASE_BOOK_URL}?api-key=${API_KEY}`;
  const displayBook = document.getElementById("books-container");
  const displayFavBook = document.getElementById("favorite-book");
  // Fetch bestselling books for date and add top 5 to page


  fetch(bookURL)
  .then(function(data) {
    return data.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);
    displayBook.innerHTML = "Display top 5 Books Here:";
    const booksList = responseJson.results.books;
    for (let index = 0; index < 10; index++) {
      const book = document.createElement("ul");
      const bookRank = booksList[index].rank;
      book.innerText = `book ${bookRank}`;
      displayBook.appendChild(book);
      const bookTitle = document.createElement("li");
      const bookAuthor = document.createElement("li");
      const bookDesc = document.createElement("li");
      bookTitle.innerText = booksList[index].title;
      bookAuthor.innerText = booksList[index].author;
      bookDesc.innerText = booksList[index].description;
      book.appendChild(bookTitle); 
      book.appendChild(bookAuthor); 
      book.appendChild(bookDesc); 
      const newBook = `Title: ${booksList[index].title} Author: ${booksList[index].author} ISBN: ${booksList[index].primary_isbn13}`;
      bookList.push(newBook);
      console.log(bookList);
      // const favoriteBooksList = document.createElement("ul");
      // displayFavBook.appendChild(favoriteBooksList);
      // const bookDesc = document.createElement("li");
      // bookTitle.innerText = booksList[index].title;
    }
  });
});

bookContainer.addEventListener("click", function (e) {
  e.preventDefault();
  const clickTarget = e.target;
  const listItem = clickTarget.closest("ul");
  const bookTitle = listItem.childNodes[1].innerText;

  const readingTitle = document.createElement("li");
  // readingTitle.innerText = bookTitle;
  readingTitle.innerHTML = `<span>${bookTitle}</span> <a href=" " class="btn btn-danger btn-sm delete">`;
  listOfReading.appendChild(readingTitle);
})

class BookList {
  static getBooks() {
    let books;
    if(localStorage.getItem('books') === null) {
      books = [];
    } else {
      books = JSON.parse(localStorage.getItem('books'));
    }

    return books;
  }

  static addBook(book) {
    const books = Store.getBooks();
    books.push(book);
    localStorage.setItem('books', JSON.stringify(books));
  }

  static removeBook(isbn) {
    const books = Store.getBooks();

    books.forEach((book, index) => {
      if(book.isbn === isbn) {
        books.splice(index, 1);
      }
    });

    localStorage.setItem('books', JSON.stringify(books));
  }
}