describe("Validate date format",() =>{
    it("If date format is valid, then return true", ()=>{
        expect(isValidDate("2010-50-15")).toEqual(false);
        expect(isValidDate("2010-10-1")).toEqual(false);
        expect(isValidDate("2010-10-01")).toEqual(true);

    });
});