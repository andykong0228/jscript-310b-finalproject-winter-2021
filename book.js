const bookFormEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');
const bookContainer = document.getElementById("books-container");
const readingList = document.getElementById("reading-container");

bookFormEl.addEventListener('submit', function(e) {
  e.preventDefault();
  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;
  const bookDate = `${year}-${month}-${date}`;
  if (!isValidDate(bookDate)) {
    alert("please enter valid date");
    return false;
  }
  const BASE_BOOK_URL =`https://api.nytimes.com/svc/books/v3/lists/${bookDate}/hardcover-fiction.json`;
  const bookURL = `${BASE_BOOK_URL}?api-key=${API_KEY}`;

  // Fetch bestselling books for date and add top 10 to page
  fetch(bookURL)
  .then(function(data) {
    return data.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);
    bookContainer.innerHTML = `<h5>Display top 10 Books Here:</h5>`;
    const booksList = responseJson.results.books;
    for (let index = 0; index < 10; index++) {
      const book = document.createElement("ul");
      book.innerHTML = `<label>Book${booksList[index].rank}:</label>`;
      bookContainer.appendChild(book);
      const bookTitle = document.createElement("li");
      const bookAuthor = document.createElement("li");
      const bookDesc = document.createElement("li");
      bookTitle.innerText = booksList[index].title;
      bookAuthor.innerText = booksList[index].author;
      bookDesc.innerText = booksList[index].description;
      book.appendChild(bookTitle); 
      book.appendChild(bookAuthor); 
      book.appendChild(bookDesc); 
    }
    showAlert('Best-book search is completed', 'success');
  });
});

// If book element is clicked, add book title for the element to reading list
bookContainer.addEventListener("click", function (e) {
  e.preventDefault();
  const clickTarget = e.target;
  const listItem = clickTarget.closest("ul");
  const tl = listItem.childNodes[1].innerText;
  const au = listItem.childNodes[2].innerText;
  //adds books to localStorage
  if (!Store.containsBook(tl)) {
    const readingBook = new Book(tl, au);  
    Store.addBook(readingBook);
  } else {
    alert("book is already in the list");
    return false;
  }
  
  //adds book to display
  addBookToList(tl);
})

//add book title to reading list
const addBookToList= (bookTitle) => {
  const newLi = document.createElement('li');
  newLi.innerHTML = `<span>${bookTitle}</span> <a class="delete text-white">Delete</a >`;
  readingList.appendChild(newLi);
  showAlert('Book is added to reading list', 'success');
}

// If delete is clicked, toggle the class "done" on the <li>
// If a delete link is clicked, delete the li element remove from the DOM and from Store
readingList.addEventListener("click", function (e) {
  const clickTarget = e.target;
  const listItem = clickTarget.closest("li");
  if (clickTarget.className === "delete") {
    //removes books from localStorage
    const title = listItem.childNodes[0].innerText;
    Store.removeBook(title);
    //removes book from display
    listItem.remove();
    showAlert('Book is removed from reading list', 'warning');
  } else {
    listItem.classList.toggle("done");
    showAlert('Reading is completed', 'success');
  }
});

// show alert on page for 3 seconds
const showAlert = (message, className) => {
    const div = document.createElement('div');
    div.innerHTML = `<h5 class = "alert alert-${className}">${message}</h5>`
    const form = document.querySelector('.col-12');
    form.appendChild(div);
  
    // Vanish in 3 seconds
    setTimeout(() => document.querySelector('.alert').remove(), 3000);
}

//addbook(book, array booklist)