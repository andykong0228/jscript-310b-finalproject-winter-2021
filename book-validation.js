function isValidDate(bookDate) {

    const dateFormat = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
    if (bookDate.match(dateFormat)) {
        return true;
    } else {
        return false;
    }
}